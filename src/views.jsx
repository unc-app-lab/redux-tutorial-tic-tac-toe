import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { winner } from './util.js'
import { newGame, move, fetchTodo } from './actions.js'

const Square = ({onClick, content}) => (
  <button className='square' onClick={onClick}>{content}</button>
)

const Row = ({onMove, squares, startIndex}) => (
  <div className='row'>
    <Square onClick={() => onMove(startIndex)}
      content={squares[startIndex    ]} />
    <Square onClick={() => onMove(startIndex + 1)}
      content={squares[startIndex + 1]} />
    <Square onClick={() => onMove(startIndex + 2)}
      content={squares[startIndex + 2]} />
  </div>
)

const Board = ({onMove, squares}) => (
  <div className='board'>
    <Row onMove={onMove} squares={squares} startIndex={0} />
    <Row onMove={onMove} squares={squares} startIndex={3} />
    <Row onMove={onMove} squares={squares} startIndex={6} />
  </div>
)

const Status = ({xIsNext, winner, onNewGame}) => {
  const text = winner === null ? 'Tie game :-/'
        : winner !== undefined ? `${winner} wins!`
        : `It is ${xIsNext ? 'X' : 'O'}'s turn.`
  return (
    <div className='status'>
      <p>{text}</p>
      <button onClick={onNewGame}>New game</button>
    </div>
  )
}

const InnerTodoThingie = ({isFetching, json, error, onFetch}) => {
  const content = isFetching ? <div>Fetching a todo&hellip;</div>
        : error ? <div>Error fetching a todo: {error}</div>
        : json ? <pre>{json}</pre>
        : ''
  const showButton = !isFetching
  const buttonLabel = json ? 'Fetch another' : 'Fetch a todo'
  const button = showButton ? <button onClick={onFetch}>{buttonLabel}</button> : null
  return (
    <div>
      <h3>Todo data</h3>
      {content}
      {button}
    </div>
  )
}

let mapStateToProps = state => state.todo
let mapDispatchToProps = dispatch => ({
  onFetch: () => dispatch(fetchTodo())
})
const TodoThingie = connect(mapStateToProps, mapDispatchToProps)(InnerTodoThingie)

const InnerApp = ({squares, xIsNext, onMove, onNewGame}) => (
  <div>
    <Board onMove={onMove} squares={squares} />
    <Status xIsNext={xIsNext} winner={winner(squares)} onNewGame={onNewGame} />
    <TodoThingie />
  </div>
)

mapStateToProps = state => ({
  squares: state.squares,
  xIsNext: state.xIsNext,
})

mapDispatchToProps = dispatch => ({
  onMove: (index) => dispatch(move(index)),
  onNewGame: () => dispatch(newGame()),
})

export const App = connect(mapStateToProps, mapDispatchToProps)(InnerApp)
