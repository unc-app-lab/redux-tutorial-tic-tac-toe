import { Actions } from './actions.js'
import { winner } from './util.js'

export const initialState = {
  squares: Array(9).fill(null),
  xIsNext: true,
  todo: {
    isFetching: false,
    json: null,
    error: null,
  }
}

const moveReducer = (state, action) => {
  const {index} = action.payload || {}
  if (index == null) return state
  const {squares, xIsNext} = state
  if (winner(squares) !== undefined) return state // game already over
  if (squares[index]) return state // square already taken
  const newSquares = squares.slice()
  newSquares[index] = xIsNext ? 'X' : 'O'
  return {
    ...state,
    squares: newSquares,
    xIsNext: !xIsNext,
  }
}

const newGameReducer = (state, action) => initialState

const fetchTodoStartReducer = (state, action) => ({
  ...state,
  todo: {
    isFetching: true,
    json: null,
    error: null,
  },
})

const fetchTodoSuccessReducer = (state, action) => ({
  ...state,
  todo: {
    isFetching: false,
    json: action.payload,
  }
})

const fetchTodoFailureReducer = (state, action) => ({
  ...state,
  todo: {
    isFetching: false,
    error: action.error,
  }
})

export const rootReducer = (state, action) => {
  if (action.type === Actions.move) {
    return moveReducer(state, action)
  } else if (action.type === Actions.newGame) {
    return newGameReducer(state, action)
  } else if (action.type === Actions.fetchTodoStart) {
    return fetchTodoStartReducer(state, action)
  } else if (action.type === Actions.fetchTodoSuccess) {
    return fetchTodoSuccessReducer(state, action)
  } else if (action.type === Actions.fetchTodoFailure) {
    return fetchTodoFailureReducer(state, action)
  } else {
    return state
  }
}
